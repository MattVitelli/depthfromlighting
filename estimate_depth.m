clear all;
close all;
clc;
cube = [-1,-1,-5;
         1, 1, 5];
width = 64;
height = 64;
f = 1.05;
K = [f*width, 0,  width*0.5;
     0, f*height, height*0.5;
     0, 0, 1];
R = eye(3);
T = zeros(3,1);
P = K * [R, T];
[Normals, Positions, Depths] = ray_trace_scene(cube, P, width, height);

M = P(:,1:3);
Minv = inv(M);

rays = ones(height, width, 3);
rays(:,:,1) = repmat(1:1:width,height,1);
rays(:,:,2) = repmat([1:1:height]',1,width);
raysTemp = rays;   
rays(:,:,1) = Minv(1,1).*raysTemp(:,:,1) + Minv(1,2).*raysTemp(:,:,2) ...
                + Minv(1,3).*raysTemp(:,:,3);
rays(:,:,2) = Minv(2,1).*raysTemp(:,:,1) + Minv(2,2).*raysTemp(:,:,2) ...
                + Minv(2,3).*raysTemp(:,:,3);
rays(:,:,3) = Minv(3,1).*raysTemp(:,:,1) + Minv(3,2).*raysTemp(:,:,2) ...
                + Minv(3,3).*raysTemp(:,:,3);
            
%GT Normals and Positions computed the Computer Graphics way            
[dPdx_x, dPdy_x] = imgradientxy(Positions(:,:,1));
[dPdx_y, dPdy_y] = imgradientxy(Positions(:,:,2));
[dPdx_z, dPdy_z] = imgradientxy(Positions(:,:,3));
dPdx(:,:,1) = dPdx_x;
dPdx(:,:,2) = dPdx_y;
dPdx(:,:,3) = dPdx_z;
dPdy(:,:,1) = dPdy_x;
dPdy(:,:,2) = dPdy_y;
dPdy(:,:,3) = dPdy_z;
N = cross(dPdy, dPdx, 3);
N = N ./ repmat(sum(N.^2,3).^0.5, [1, 1, 3]);
estPos = rays .* repmat(Depths, [1, 1, 3]);


%Closed form linear approximation for normal in terms of neighboring depths
for i = 1:3
ray_right(:,:,i) = [rays(:,2:width,i), zeros(height,1)];
ray_left(:,:,i) = [zeros(height,1), rays(:,1:width-1,i)];
ray_up(:,:,i) = [rays(2:height,:,i); zeros(1, width)];
ray_down(:,:,i) = [zeros(1, width); rays(1:height-1,:,i)];
end
depth_right = [Depths(:,2:width), zeros(height,1)];
depth_left = [zeros(height,1), Depths(:,1:width-1)];
depth_up = [Depths(2:height,:); zeros(1,width)];
depth_down = [zeros(1, width); Depths(1:height-1, :)];
            
dPdx_closed = (ray_right .* repmat(depth_right, [1,1,3]) - ray_left .* repmat(depth_left, [1,1,3]))*0.5;
dPdy_closed = (ray_up .* repmat(depth_up, [1,1,3]) - ray_down .* repmat(depth_down, [1,1,3]))*0.5;

N_closed_form = cross(dPdy_closed, dPdx_closed, 3);
N_closed_form = N_closed_form ./ repmat(sum(N_closed_form.^2,3).^0.5, [1, 1, 3]);
%Closed form solution has an error of roughly 0.27% MSE across all 3
%components. We use central differencing to make the gradients easier to
%derive in terms of d_xy


%Now the fun part:
%Let Pi, Pj represent the positions of two lights
%Let Ii, Ij represent the images seen with each light on individually so
%that Ik = AkB*N'*(Pk - d_dxy.*K'[x;y;1])* ...
%((Pk - d_dxy.*K'[x;y;1])'*(Pk - d_dxy.*K'[x;y;1]))^(-3/2) <-- The 1.0/Rk^3
%term
%Assuming Ak is constant for all lights (think of this as light attenuation
%factor), then Ii/Ij = (N'*((Pi - d_dxy.*K'[x;y;1])*Rj^3)/(N'((Pj - d_dxy.*K'[x;y;1])*Ri^3)
%Want to minimize J(d_xy) = 0.5*(Ii/Ij - (N'*((Pi - d_dxy.*K'[x;y;1])*Rj^3)/(N'((Pj - d_dxy.*K'[x;y;1])*Ri^3)).^2
%w.r.t d_xy
%dJ/dd_xy = d_d_xy on quadratic term - d_d_xy on Ii/Ij*(N'*((Pi - d_dxy.*K'[x;y;1])*Rj^3)/(N'((Pj - d_dxy.*K'[x;y;1])*Ri^3))
%((N'*((Pi - d_dxy.*K'[x;y;1])*Rj^3)/(N'((Pj - d_dxy.*K'[x;y;1])*Ri^3)))^2
%r = K^-1*[x;y;1]
%f(d_dxy) = N'*(Pi - dr)
%g(d_dxy) = ((Pi - dr)'*(Pi - dr))^(-3/2)
%h(d_dxy) = (N'*(Pj - dr))^-1
%e(d_dxy) = ((Pj - dr)'*(Pj - dr))^(3/2)
%J(d_xy) = 0.5*(Ii/Ij - f(d_dxy)g(d_dxy)h(d_dxy)e(d_dxy))^2
lights = [-0.1, 0, 0;
           0.1, 0, 0;
           0.0, 0.1, 0;
           0.0, -0.1, 0;];
%lights = randn(10,3)*0.03;
observations = {};
num_lights = size(lights, 1)
num_safety_factor = 1.0e-4;
for i = 1 : num_lights
    Pi = zeros(1,1,3);
    Pi(1,1,1) = lights(i,1);
    Pi(1,1,2) = lights(i,2);
    Pi(1,1,3) = lights(i,3);
    Pi = repmat(Pi, [height, width, 1]);
    toLight = Pi - Positions;
    dist = sum(toLight.^2,3).^0.5;
    toLight = toLight ./ repmat(dist, [1,1,3]);
    observations{i} = max(1.0/255.0, 8.0*max(sum(toLight.*Normals,3),0.0)./(num_safety_factor+dist.^2));
    %imshow(observations{i});
    %pause;
end
%pause;
learning_rate = 0.005;
num_iterations = 600;
[est_depth, J] = sgd_depth(learning_rate, width, height, lights, observations, rays, num_iterations);
min_d = min(min(est_depth));
max_d = max(max(est_depth));
%est_depth = (est_depth - min_d) / (max_d - min_d);
min_d = min(min(Depths));
max_d = max(max(Depths));
%Depths = (Depths - min_d) / (max_d - min_d);

figure();
plot(J);


figure();
num_figs = 4;
subplot(num_figs,1,1);
imshow(abs(Normals));
subplot(num_figs,1,2);
imshow(Positions);
subplot(num_figs,1,3);
imshow(Depths/5);
subplot(num_figs,1,4);
imshow(est_depth/5);%(N_closed_form - Normals).^2);
errorD = mean(mean((est_depth - Depths).^2))^0.5
%errorN = mean(mean(mean((N_closed_form - Normals).^2)))
%error = mean(mean(mean((estPos - Positions).^2)))
