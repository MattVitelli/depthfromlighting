function [ Normals, Positions, Depths ] = ray_trace_scene( cube, P, width, height)
%ray_trace_scene - Ray traces scene from camera perspective
%Inputs:
% cube - Box representing room layout
% P - Camera projection matrix
% width - Width of output images
% height - Height of output images
%Outputs:
% Normals - Normal map of scene
% Positions - Position map of scene
   TINY = 1.0e-7;
   INFINITY = 1.0e15;

   M = P(:,1:3);
   p4 = P(:,4);
   
   Positions = zeros(height, width, 3);
   Normals = zeros(height, width, 3);
   
   Minv = inv(M);
   rayOrigin = -Minv*p4;
   
   halfScale = [max(cube(:,1))-min(cube(:,1));
            max(cube(:,2))-min(cube(:,2));
            max(cube(:,3))-min(cube(:,3));] * 0.5;
        
   center = [max(cube(:,1))+min(cube(:,1));
            max(cube(:,2))+min(cube(:,2));
            max(cube(:,3))+min(cube(:,3));] * 0.5;
   
   planes = [0,-1,0,halfScale(2)+center(2);
             0, 1,0,halfScale(2)-center(2);
             -1, 0,0,halfScale(1)+center(1);
             1, 0,0,halfScale(1)-center(1);
             0, 0,-1,halfScale(3)+center(3);
             0, 0,1,halfScale(3)-center(3);];
   %Define which position components should be used for irradiance lookups
   %Note - The indices should be consistent with the planes above and
   %the array representation used in radiosity_solver.m
   plane_lookups = {};
   plane_lookups{1} = [1,3]; %Lookup on XZ plane
   plane_lookups{2} = [1,3]; %Lookup on XZ plane
   plane_lookups{3} = [2,3]; %Lookup on YZ plane
   plane_lookups{4} = [2,3]; %Lookup on YZ plane
   plane_lookups{5} = [1,2]; %Lookup on XY plane
   plane_lookups{6} = [1,2]; %Lookup on XY plane
   
   %Note - the following code is not particularly intuitive or readable,
   %but essentially we're creating an array of camera rays that will be
   %used to ray-trace a scene of a cube
   rays = ones(height, width, 3);
   rays(:,:,1) = repmat(1:1:width,height,1);
   rays(:,:,2) = repmat([1:1:height]',1,width);
   raysTemp = rays;
   
   rays(:,:,1) = Minv(1,1).*raysTemp(:,:,1) + Minv(1,2).*raysTemp(:,:,2) ...
                + Minv(1,3).*raysTemp(:,:,3);
   rays(:,:,2) = Minv(2,1).*raysTemp(:,:,1) + Minv(2,2).*raysTemp(:,:,2) ...
                + Minv(2,3).*raysTemp(:,:,3);
   rays(:,:,3) = Minv(3,1).*raysTemp(:,:,1) + Minv(3,2).*raysTemp(:,:,2) ...
                + Minv(3,3).*raysTemp(:,:,3);
   rays(:,:,:) = rays(:,:,:) ./ repmat(rays(:,:,3),[1,1,3]); %Normalize rays
   
   minDist = ones(height, width)*INFINITY;
   Nx = zeros(height, width);
   Ny = zeros(height, width);
   Nz = zeros(height, width);
   closest_plane = zeros(height, width);
   %Perform ray plane intersection for each plane and take nearest
   %intersection
   for i=1:size(planes,1)
       dist = planes(i,1)*rays(:,:,1)+planes(i,2)*rays(:,:,2) ... 
           + planes(i,3)*rays(:,:,3);
       validIntersections = abs(dist)>TINY;
       dist(validIntersections) = -(planes(i,1:3)*rayOrigin + planes(i,4)) ./ dist(validIntersections);
       validIntersections = dist > 0.0 & dist < minDist;
       minDist(validIntersections) = dist(validIntersections);
       Nx(validIntersections) = planes(i,1);
       Ny(validIntersections) = planes(i,2);
       Nz(validIntersections) = planes(i,3);
       closest_plane(validIntersections) = i;
   end
   rayO = [];
   rayO(1,1,1:3) = rayOrigin;
   Normals(:,:,1) = Nx;
   Normals(:,:,2) = Ny;
   Normals(:,:,3) = Nz;
   Positions(:,:,:) = repmat(rayO, [height, width, 1]) + rays(:,:,:) .* repmat(minDist,[1,1,3]);
   Depths = minDist;
end