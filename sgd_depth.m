function [ DepthEst, J ] = sgd_depth( lr, width, height, lights, observations, rays, num_iters)
%SGD_DEPTH Summary of this function goes here
%   Detailed explanation goes here

decay_rate = 0.9
lambda = 0.0%5;%5;%01;%5;%25;
randInit = randn(height/8,width/8);
randInit = imresize(randInit, 8);
DepthEst = ones(height, width);% + randn(height,width)*0.3;
num_safety_factor = 1.0e-4;
brightness = 8.0;
for i = 1:3
ray_right(:,:,i) = [rays(:,2:width,i), zeros(height,1)];
ray_left(:,:,i) = [zeros(height,1), rays(:,1:width-1,i)];
ray_up(:,:,i) = [rays(2:height,:,i); zeros(1, width)];
ray_down(:,:,i) = [zeros(1, width); rays(1:height-1,:,i)];
end
num_lights = size(lights, 1);
r = rays;
diff_h = 1.0e-8;
vel = zeros(height, width);
for it = 1 : num_iters
    num_evals = 0;
    dJ_dd_total = zeros(height, width);
    %Regular Depth Parameterization
    %depth_right = [DepthEst(:,2:width), zeros(height,1)];
    %depth_left = [zeros(height,1), DepthEst(:,1:width-1)];
    %depth_up = [DepthEst(2:height,:); zeros(1,width)];
    %depth_down = [zeros(1, width); DepthEst(1:height-1, :)];
    
    %Inverse Depth Parameterization
    depth_right = [DepthEst(:,2:width), ones(height,1)];
    depth_left = [ones(height,1), DepthEst(:,1:width-1)];
    depth_up = [DepthEst(2:height,:); ones(1,width)];
    depth_down = [ones(1, width); DepthEst(1:height-1, :)];
            
    dPdx_closed = (ray_right ./ repmat(depth_right, [1,1,3]) - ray_left ./ repmat(depth_left, [1,1,3]))*0.5;
    dPdy_closed = (ray_up ./ repmat(depth_up, [1,1,3]) - ray_down ./ repmat(depth_down, [1,1,3]))*0.5;
    DepthEstp = DepthEst+diff_h;
    DepthEstm = DepthEst-diff_h;
    dr = repmat(DepthEst, [1,1,3]).*rays;
    drp = rays ./ repmat(DepthEstp, [1,1,3]);
    drm = rays ./ repmat(DepthEstm, [1,1,3]);

N_closed_form = cross(dPdy_closed, dPdx_closed, 3);
%Normalization doesn't matter, as it gets cancelled out by division
N_closed_form = N_closed_form ./ repmat(sum(N_closed_form.^2,3).^0.5, [1, 1, 3]);
Je = 0.0;
for i = 1:num_lights
    Pi = zeros(1,1,3);
    Pi(1,1,1) = lights(i,1);
    Pi(1,1,2) = lights(i,2);
    Pi(1,1,3) = lights(i,3);
    Pi = repmat(Pi, [height, width, 1]);
    %for j = i+1:num_lights
    for j = num_lights-1:num_lights;
        Pj = zeros(1,1,3);
        Pj(1,1,1) = lights(j,1);
        Pj(1,1,2) = lights(j,2);
        Pj(1,1,3) = lights(j,3);
        Pj = repmat(Pj, [height, width, 1]);
        
        %State needed for cost + gradients
        y = observations{i} ./ observations{j};
        %{
        f = sum(N_closed_form.*(Pi - dr),3);
        g = sum((Pi - dr).*(Pi - dr),3).^(-3.0/2.0);
        h = sum(N_closed_form.*(Pj - dr),3).^(-1);
        e = sum((Pj - dr).*(Pj - dr),3).^(3.0/2.0);
        
        fp = sum(N_closed_form.*(Pi - drp),3);
        gp = sum((Pi - drp).*(Pi - drp),3).^(-3.0/2.0);
        hp = sum(N_closed_form.*(Pj - drp),3).^(-1);
        ep = sum((Pj - drp).*(Pj - drp),3).^(3.0/2.0);
        
        fm = sum(N_closed_form.*(Pi - drm),3);
        gm = sum((Pi - drm).*(Pi - drm),3).^(-3.0/2.0);
        hm = sum(N_closed_form.*(Pj - drm),3).^(-1);
        em = sum((Pj - drm).*(Pj - drm),3).^(3.0/2.0);
        %}
        
        toLight = Pi - drp;
        dist = sum(toLight.^2,3).^0.5;
        toLight = toLight ./ repmat(dist, [1,1,3]);
        guess_ip = max(1.0/255.0, brightness*max(sum(toLight.*N_closed_form,3),0.0)./(num_safety_factor+dist.^2));
        toLight = Pj - drp;
        dist = sum(toLight.^2,3).^0.5;
        toLight = toLight ./ repmat(dist, [1,1,3]);
        guess_jp = max(1.0/255.0, brightness*max(sum(toLight.*N_closed_form,3),0.0)./(num_safety_factor+dist.^2));
        
        toLight = Pi - drm;
        dist = sum(toLight.^2,3).^0.5;
        toLight = toLight ./ repmat(dist, [1,1,3]);
        guess_im = max(1.0/255.0, brightness*max(sum(toLight.*N_closed_form,3),0.0)./(num_safety_factor+dist.^2));
        toLight = Pj - drm;
        dist = sum(toLight.^2,3).^0.5;
        toLight = toLight ./ repmat(dist, [1,1,3]);
        guess_jm = max(1.0/255.0, brightness*max(sum(toLight.*N_closed_form,3),0.0)./(num_safety_factor+dist.^2));
        
        hypothesisp = guess_ip./guess_jp;
        hypothesism = guess_im./guess_jm;
        y = observations{i};
        hypothesisp = guess_ip;
        hypothesism = guess_im;
        J_regp = 0.5*(depth_right - DepthEstp).^2 + 0.5*(depth_right - DepthEstp).^2 + 0.5*(depth_up - DepthEstp).^2 + 0.5*(depth_down - DepthEstp).^2;
        J_regm = 0.5*(depth_right - DepthEstm).^2 + 0.5*(depth_right - DepthEstm).^2 + 0.5*(depth_up - DepthEstm).^2 + 0.5*(depth_down - DepthEstm).^2;
        J_regp = J_regp + 0.5 * (DepthEstp).^2;
        J_regm = J_regm + 0.5 * (DepthEstm).^2;
        Jp = 0.5*(y - hypothesisp).^2 + lambda*J_regp;
        Jm = 0.5*(y - hypothesism).^2 + lambda*J_regm;

        %{
        dfddxy = -sum(N_closed_form.*r,3);
        dhddxy = sum(N_closed_form.*r,3).^(-2);
        dgddxy = -(3.0/2.0)*sum(2.0*dr.*r - 2.0*Pi.*r,3).*sum((Pi - dr).*(Pi - dr),3).^(-5.0/2.0);
        deddxy = (3.0/2.0)*sum(2.0*dr.*r - 2.0*Pj.*r,3).*sum((Pj - dr).*(Pj - dr),3).^(1.0/2.0);

        t0 = (g.^2).*(h.^2).*(e.^2);
        t1 = (f.^2).*(h.^2).*(e.^2);
        t2 = (f.^2).*(g.^2).*(e.^2);
        t3 = (f.^2).*(g.^2).*(h.^2);

        s0 = g.*h.*e;
        s1 = f.*h.*e;
        s2 = f.*g.*e;
        s3 = f.*g.*h;

        %Expanded error for clarity
        %J = 0.5*(y*y + f*f*g*g*h*h*e*e - 2.0*y*f.*g.*h.*e)

        %Quadratic Term
        dJ_dd_quad      = dfddxy.*f.*t0 + dgddxy.*g.*t1 + dhddxy.*h.*t2 + deddxy.*e.*t3;
        %Data-Coupled Term
        dJ_dd_coupling  = -y.*(dfddxy.*s0 + dgddxy.*s1 + dhddxy.*s2 + deddxy.*s3);
        dJ_dd = dJ_dd_quad + dJ_dd_coupling;
        %}
        dJ_dd = (Jp - Jm)/(2.0*diff_h);
        dJ_dd_total = dJ_dd_total + dJ_dd;
        
        %Error Term
        Je = Je + 0.5*mean(mean((Jp+Jm)*0.5));
        num_evals = num_evals+1;
    end
    
end
        Je = Je / num_evals
        it
        J(it) = Je;
        dJ_dd_total = dJ_dd_total / num_evals;
        vel = decay_rate * vel + (1.0-decay_rate) * dJ_dd_total.^2;
        %lr = lr * 0.999;
        DepthEst = DepthEst -lr*dJ_dd_total./((1.0e-8 + vel).^0.5);
        
        %imshow(DepthEst);
        %pause;

end
    DepthEst = 1.0 ./ DepthEst;
end

